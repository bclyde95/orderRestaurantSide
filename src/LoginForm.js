import {Component} from "react";
import React from "react";

class LoginForm extends Component{
    state = {
        username: "",
        password: ""
    }

    onChange(e){
        this.setState({[e.target.name]: e.target.value});
    }

    onSubmit(e){
        this.props.onSendLogin(this.state.username, this.state.password);
        this.setState({
            username: "",
            password: ""
        })
        e.preventDefault();
    }

    render () {
        return (
            <div className="LoginForm">
                <form onSubmit={e => this.onSubmit(e)}>
                    <input
                        onChange={e => this.onChange(e)}
                        value={this.state.username}
                        name="username"
                        type="text"
                        placeholder="Username"
                        autofocus="true"
                    />
                    <input
                        onChange={e => this.onChange(e)}
                        value={this.state.password}
                        name="password"
                        type="password"
                        placeholder="Password"
                    />
                    <button>Send</button>
                </form>
            </div>
        );
    }
}

export default LoginForm
