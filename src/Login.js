import {Component} from 'react';
import React from 'react';

import LoginForm from './LoginForm.js'

class Login extends Component{

    checkResponseStatus= (response) => {
        if (response.status >=200 && response.status <= 300){
            return response
        } else{
            let error = new Error(response.statusText)
            error.response = response
            throw error
        }
    }

    getData = (response) => {
        return response.json()
    }

    onSendLogin= (username, password) => {
        output = {
            username: username,
            password: password
        }

        fetch('http://127.0.0.1:5000/api/auth/restaurantlogin', {
            method: "POST",
            body: output
        }).then(this.checkStatus).then(this.getData).then((data) => {
            return data
        });
    }

    render(){
        return (
            <div className="Login">
                <LoginForm onSendLogin={this.onSendLogin} />
            </div>
        );
    }
}

