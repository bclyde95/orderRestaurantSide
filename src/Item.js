import {Component} from 'react';
import React from 'react';

class Item extends Component {
    plural = (item) => {
        if (item.quantity > 1)
            return item.item + "s"
        else
            return item.item
    }

    render () {
        const item = this.props.item

        return (
            <div className="Item">
                <h3>{item.quantity} {this.plural(item)}</h3>
                <div className="ingredients">
                    <h4>Ingredients</h4>
                    <ul>
                        {item.ingredients.map((ingredient) => {
                            return <li><span className="key">{Object.keys(ingredient)[0]}</span> : {ingredient[Object.keys(ingredient)[0]]}</li>
                        })}
                    </ul>
                </div>
                <div className="options">
                    <h4>Options</h4>
                    <ul>
                        {item.options.map((option) => {
                            return <li><span className="key">{Object.keys(option)[0]}</span> : {option[Object.keys(option)[0]]}</li>
                        })}
                    </ul>
                </div>
            </div>
        );
    }
}

export default Item;
