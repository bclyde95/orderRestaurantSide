import {Component} from 'react';
import React from 'react';
import Item from './Item.js';

class Order extends Component{
    hasPaid=(order) => {
        if (order.paid === true)
            return <b className='good'>Yes</b>
        else
            return <b className='bad'>No</b>
    }

    render(){
        const order = this.props.order;
		const noteStyle = {
			fontSize: "16px"
		}
        return (
            <div className="OrderCard">
                <div className="customer">
                    <h1>{order.customerName}</h1>
                    <p><span className="key">Phone</span>: {order.customerPhone}</p>
                    <p><span className="key">Paid</span>: {this.hasPaid(order)}</p>
                    <p className="key">{order.date} &nbsp;{order.time}</p>
                    <h4>Notes</h4>
                    {order.notes.map((note) => {
                        return <p style={noteStyle}>{note}</p>    
                    })}
                </div>
            
                <div className="order">
                    <h2>Order</h2>
                    {order.order.items.map((item) => {
                        return <Item item={item} />
                    })}
                </div>
            </div>
        );
    }
}

export default Order;
