import React, { Component } from 'react';
import './App.css';
import Order from './Order.js';
var data = require('./example.json');

class App extends Component {

  render() {
    return (
      <div className="App">
        <div className="OrderContainer">
            {data.map((order) => {
                return <Order order={order} />
            })}
        </div>
      </div>
    );
  }
}

export default App;
