const fs = require('fs');

fs.readFile('./example.json', (err,data) => {
    if (err)
        throw err;

    const obj = JSON.parse(data);
    const item = obj.order.items[0];
    obj.order.items.map((item) => {
        if (item.quantity > 1)
            console.log(item.quantity + " " + item.item + "s");
        else
            console.log(item.quantity + " " + item.item);

        // map ingredients list
        console.log("Ingredients")
        item.ingredients.map((ingredient, i) => {
            console.log("  " + Object.keys(ingredient)[0] + " : " + ingredient[Object.keys(ingredient)[0]]);
        });

        // map options list
        console.log("Options")
        item.options.map((option, i) => {
            console.log("  " + Object.keys(option)[0] + " : " + option[Object.keys(option)[0]]);
        });
        console.log("");
    });
})
